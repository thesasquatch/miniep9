function multiplica(a, b)
	dima = size(a)
	dimb = size(b)
	if dima[2] != dimb[1]
		return -1
	end
	c = zeros(dima[1], dimb[2])
	for i in 1:dima[1]
		for j in 1:dimb[2]
			for k in 1:dima[2]
				c[i, j] = c[i, j] + a[i, k] * b[k, j]
			end
		end
	end
	return c
end

function exp_by_squaring(M, p, y)
	if p < 0
		return "Erro"
	elseif p == 0
		return Matrix{Float64}(I, size(M))
	elseif p == 1
		return M*y
	elseif ((p > 0) && (p % 2 == 0))
		return exp_by_squaring(multiplica(M,M), p/2, y)
	elseif ((p > 0) && (p % 2 == 1))
		return exp_by_squaring(multiplica(M,M),((p-1)/2), M*y)
	end
end

function matrix_pot_by_squaring(M, p)
	return exp_by_squaring(M, p, 1)
end