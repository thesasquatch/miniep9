using LinearAlgebra
using Test
function test_by_squaring()
	println("Início dos testes automatizados.")
		@test matrix_pot_by_squaring([1 2 ; 3 4], 1) == [1 2 ; 3 4]
		@test matrix_pot_by_squaring([1 2 ; 3 4], 2) == [7 10 ; 15 22]
		@test matrix_pot_by_squaring([1 2 ; 3 4], 0) == Matrix{Int}(I,2,2)
        @test matrix_pot_by_squaring([5 4 ; 0 1], 3) == [125 124 ; 0 1]
        @test matrix_pot_by_squaring([2 8 ; 2 9], 2) == [20 88 ; 22 97]
        @test matrix_pot_by_squaring([1 2 ; 3 4], 3) == [37 54 ; 81 118]
        @test matrix_pot_by_squaring([1 2 1 ; 2 2 2 ; 3 3 3], 2) == [8 9 8 ; 12 14 12 ; 18 21 18]
        @test matrix_pot_by_squaring([5 1 ; 7 0], 4) == [1199 195 ; 1365 224]
        @test matrix_pot_by_squaring([3 1 2; 4 2 4 ; 6 0 0], 1) == [3 1 2 ; 4 2 4 ; 6 0 0]
        @test matrix_pot_by_squaring(zeros(1, 1), 7) == zeros(1, 1)
        @test matrix_pot_by_squaring(ones(2, 2), 2) == [2 2 ; 2 2]
        @test matrix_pot_by_squaring([4 0 2 ; 5 1 8 ; 6 0 3], 3) == [196 0 98 ; 549 1 280 ; 294 0 147]
        @test matrix_pot_by_squaring([2 8 ; 9 5], 4) == [9304 9688 ; 10899 12937]
    println("Fim dos testes automatizados.")
	println("Início dos testes dinâmicos.")
	for i = 1:5
		dim = rand(2:10)
		a = rand(1:100, dim, dim)
		@test matrix_pot_by_squaring(a,2) == a*a
	end
	println("Fim dos testes dinâmicos.")
	println("Fim dos testes.")
end

test_by_squaring()