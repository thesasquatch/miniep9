function compare_times()
	M = Matrix(LinearAlgebra.I, 30, 30)
	@time matrix_pot(M, 10)
	@time matrix_pot_by_squaring(M, 10)
end

compare_times()

# Resposta
# Primeiro resultado de compare_times() :
# 0.000575 seconds (9 allocations: 64.688 KiB)
# 0.000275 seconds (5 allocations: 35.938 KiB)
#
# Segundo resultado de compare_times() :
# 0.000612 seconds (9 allocations: 64.688 KiB)
# 0.000193 seconds (5 allocations: 35.938 KiB)
#
# Terceiro resultado de compare_times() :
# 0.000551 seconds (9 allocations: 64.688 KiB)
# 0.000306 seconds (5 allocations: 35.938 KiB)
#
# A diferença no tempo é um fenômeno decorrente da redução das 
# iterações a cada procedimento, isto é, enquanto que na função 
# matrix_pot() são feitas todas P iterações, na função 
# matrix_pot_by_squaring(), para cada P par, temos P/2 no próximo 
# procedimento e, para cada P ímpar, temos P-1/2. Isso representa, 
# justamente, a composição de um dado P decimal na base binária. Logo,
# um dado P em binario Bp..B0, os ímpares são aqueles cujos Bi, 
# 0 <= i < p, são 1, e os pares, 0. 